/// <reference path="Lib/phaser/phaser.d.ts"/>
///<reference path="States/Boot.ts"/>
///<reference path="States/Preload.ts"/>
///<reference path="States/MainMenu.ts"/>
///<reference path="States/Game.ts"/>
///<reference path="States/GameOver.ts"/>
///<reference path="States/Statistics.ts"/>

module UniGame {
    class UniGame extends Phaser.Game {
        constructor(width?:number, height?:number) {
            super(width, height, Phaser.AUTO, 'phaser-div', {create: this.create});
        }

        create() {
            this.state.add("Boot", Boot, true);
            this.state.add("Preload", Preload, true);
            this.state.add("MainMenu", MainMenu, true);
            this.state.add("Game", Game, true);
            this.state.add("GameOver", GameOver, true);
            this.state.add("Statistics", Statistics, true);

            this.state.start("Boot");
        }
    }

    window.onload = () => {
        new UniGame(1280,720);
    };
}
