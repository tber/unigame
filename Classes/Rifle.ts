/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Bullet.ts"/>
/// <reference path="../Classes/Player.ts"/>

module UniGame {
    export class Rifle extends Gun {

        name:string;
        bulletSprite:string;
        damage:number;
        fireRate:number;
        maxBulletCount:number;
        reloadSpeed:number;
        bulletSpeed:number;
        bulletAccuracy:number;

        constructor(game:Phaser.Game, player:Player) {
            this.name = "Rifle";
            this.bulletSprite = 'bullet2';
            this.damage = 17;
            this.fireRate = 100;
            this.maxBulletCount = 30;
            this.reloadSpeed = 2000;
            this.bulletSpeed = 900;
            this.bulletAccuracy = 3;
            super(game, this.name, this.bulletSprite, this.damage, this.fireRate, this.maxBulletCount, this.reloadSpeed, this.bulletSpeed, this.bulletAccuracy, player);
        }
    }
}