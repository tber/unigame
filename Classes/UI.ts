/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Player.ts"/>
/// <reference path="../Classes/HealthBar.ts"/>

module UniGame {
    export class UI {

        game:Phaser.Game;
        healthBar:HealthBar;

        killIcon:Phaser.Sprite;
        killCount:Phaser.Text;

        gun:Phaser.Sprite;
        ammo:Phaser.Sprite;
        ammoText:Phaser.Text;
        clip:Phaser.Sprite;
        clipText:Phaser.Text;

        constructor(game:Phaser.Game) {
            this.game = game;
            this.healthBar = new HealthBar(game);

            this.killIcon = this.game.add.sprite(75, 75, "skull");
            this.killIcon.anchor.set(0.5);
            this.killIcon.scale.set(0.4);

            var style = {font: "28px Arial", fill: "#B7A448"};
            this.killCount = this.game.add.text(150, 75, "", style);
            this.killCount.anchor.set(0.5);
            this.killCount.stroke = "rgba(0,0,0,0.6)";
            this.killCount.strokeThickness = 5;

            this.gun = this.game.add.sprite(880, 680, "ammo");
            this.gun.anchor.set(0.5);
            this.gun.scale.set(0.25);

            this.ammo = this.game.add.sprite(1035, 680, "ammo");
            this.ammo.scale.set(0.4);
            this.ammo.anchor.set(0.5);

            var style = {font: "22px Arial", fill: "#ffffff"};
            this.ammoText = this.game.add.text(990, 680, "", style);
            this.ammoText.anchor.set(0.5);
            this.ammoText.stroke = "rgba(0,0,0,0.7)";
            this.ammoText.strokeThickness = 3;

            this.clip = this.game.add.sprite(1150, 680, "Clip");
            this.clip.anchor.set(0.5);
            this.clip.scale.set(0.15);

            var style = {font: "22px Arial", fill: "#ffffff"};
            this.clipText = this.game.add.text(1100, 680, "", style);
            this.clipText.anchor.set(0.5);
            this.clipText.stroke = "rgba(0,0,0,0.7)";
            this.clipText.strokeThickness = 3;

            this.killIcon.fixedToCamera = true;
            this.killCount.fixedToCamera = true;
            this.gun.fixedToCamera = true;
            this.ammo.fixedToCamera = true;
            this.ammoText.fixedToCamera = true;
            this.clip.fixedToCamera = true;
            this.clipText.fixedToCamera = true;
        }

        update(player:Player) {
            this.healthBar.update(player);

            this.killCount.setText(player.killCount.toString());

            this.gun.loadTexture(player.gun.name);
            this.ammoText.setText(player.gun.bulletCount.toString());
            if (player.gun.name === 'Pistol') {
                this.clipText.setText("Inf.");
            }
            else {
                this.clipText.setText(player.gun.clipCount.toString());
            }

            this.killIcon.bringToTop();
            this.killCount.bringToTop();
            this.gun.bringToTop();
            this.ammo.bringToTop();
            this.ammoText.bringToTop();
            this.clip.bringToTop();
            this.clipText.bringToTop();
        }
    }
}