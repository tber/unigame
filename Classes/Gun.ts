/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Bullet.ts"/>
/// <reference path="../Classes/Player.ts"/>
/// <reference path="../Classes/Unit.ts"/>

module UniGame {
    export class Gun {
        game:Phaser.Game;
        name:string;
        bulletSprite:string;
        damage:number;
        fireRate:number;
        maxBulletCount:number;
        bulletCount:number;
        clipCount:number;
        reloadTime:number;
        reloading:boolean;
        nextFire:number;
        nextReload:number;
        bulletSpeed:number;
        bulletAccuracy:number;

        firedBullets:Array<Bullet>;

        owner:Unit;

        shotSound:Phaser.Sound;

        constructor(game:Phaser.Game, name:string, bulletSprite:string, damage:number, fireRate:number, maxBulletCount:number,
                    reloadTime:number, bulletSpeed:number, bulletAccuracy:number, owner:Unit) {
            this.game = game;
            this.bulletSprite = bulletSprite;
            this.damage = damage;
            this.fireRate = fireRate;
            this.name = name;
            this.maxBulletCount = maxBulletCount;
            this.bulletCount = this.maxBulletCount;
            this.reloadTime = reloadTime;
            this.bulletSpeed = bulletSpeed;
            this.bulletAccuracy = bulletAccuracy;
            this.clipCount = 0;

            this.reloading = false;
            this.nextReload = this.game.time.now;
            this.nextFire = this.game.time.now;

            this.owner = owner;

            this.firedBullets = [];


            this.shotSound = this.game.add.audio(this.name + "Shot", 0.1, false);
        }

        update(enemies:Array<Unit>, player:Unit) {
            if (this.game.input.activePointer.isDown) {
                this.fire();
            }

            if (this.game.time.now > this.nextReload && this.bulletCount <= 0 && this.reloading) {
                this.bulletCount = this.maxBulletCount;
                this.clipCount--;
                this.reloading = false;
            }

            this.updateBullets(enemies, player);
        }

        fire() {
            if (this.game.time.now > this.nextFire) {
                this.nextFire = this.game.time.now + this.fireRate;
                if (this.bulletCount > 0) {
                    this.shotSound.play();
                    var accuracyRotation = (Math.PI / 180) * this.rng(-this.bulletAccuracy, this.bulletAccuracy);
                    this.firedBullets.push(new Bullet(this.game, this.bulletSprite, this.bulletSpeed, this.damage, this.owner, accuracyRotation));

                    this.bulletCount--;
                }
                if (this.bulletCount <= 0 && !this.reloading) {
                    this.reload();
                }
            }
        }

        reload() {
            if (this.clipCount > 0 && !this.reloading) {
                this.nextReload = this.game.time.now + this.reloadTime;
                this.reloading = true;
            }
        }

        updateBullets(enemies:Array<Unit>, player:Unit) {
            this.firedBullets.forEach((bullet)=> {
                bullet.update(enemies, player);
                if (bullet.hasHit) {
                    bullet.sprite.destroy();
                    this.firedBullets.splice(this.firedBullets.lastIndexOf(bullet), 1);
                }
            });
        }

        rng(from, to) {
            return Math.round(Math.random() * (to - from) + from);
        }
    }
}