/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Player.ts"/>
/// <reference path="../Classes/Unit.ts"/>
/// <reference path="../Classes/Enemy.ts"/>

module UniGame {
    export class Bullet {
        game:Phaser.Game;
        sprite:Phaser.Sprite;
        speed:number;
        damage:number;
        hasHit:boolean;

        constructor(game:Phaser.Game, sprite:string, speed:number, damage:number, owner:Unit, rotationOffset?:number) {
            this.game = game;
            this.sprite = this.game.add.sprite(0, 0, sprite);
            this.sprite.anchor.set(0.5);
            this.sprite.scale.set(0.5);
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.body.setSize(35, 35);

            this.speed = speed;
            this.damage = damage;
            this.hasHit = false;

            this.sprite.rotation = owner.sprite.rotation + rotationOffset;
            this.sprite.reset(owner.sprite.x + 55 * Math.cos(this.sprite.rotation), owner.sprite.y + 55 * Math.sin(this.sprite.rotation));
        }

        update(enemies:Array <Unit>, player:Unit) {
            if (this.sprite && (this.sprite.x < 0 || this.sprite.x > this.game.world.bounds.width ||
                this.sprite.y < 0 || this.sprite.y > this.game.world.bounds.height)) {
                this.hasHit = true;
            }
            else if (this.sprite) {
                enemies.forEach((enemy)=> {
                    if (!enemy.isDead) {
                        this.checkUnitCollision(enemy);
                    }
                });
                this.checkUnitCollision(player);
            }
            this.game.physics.arcade.velocityFromRotation(this.sprite.rotation, this.speed, this.sprite.body.velocity);
        }

        checkUnitCollision(unit:Unit) {
            if (this.game.physics.arcade.intersects(this.sprite.body, unit.sprite.body) && !this.hasHit) {
                unit.takeDamage(this.damage);
                this.hasHit = true;
            }
        }
    }
}