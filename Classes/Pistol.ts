/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Bullet.ts"/>
/// <reference path="../Classes/Player.ts"/>

module UniGame {
    export class Pistol extends Gun {
        name:string;
        bulletSprite:string;
        damage:number;
        fireRate:number;
        maxBulletCount:number;
        reloadSpeed:number;
        bulletSpeed:number;
        bulletAccuracy:number;

        constructor(game:Phaser.Game, player:Player) {
            this.name = "Pistol";
            this.bulletSprite = 'bullet3';
            this.damage = 20;
            this.fireRate = 800;
            this.maxBulletCount = 12;
            this.reloadSpeed = 1500;
            this.bulletSpeed = 900;
            this.bulletAccuracy = 1;
            super(game, this.name, this.bulletSprite, this.damage, this.fireRate, this.maxBulletCount, this.reloadSpeed, this.bulletSpeed, this.bulletAccuracy, player);
        }

        reload() {
            this.clipCount++;
            super.reload();
        }
    }
}
