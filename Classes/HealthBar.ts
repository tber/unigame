/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Player.ts"/>

module UniGame {
    export class HealthBar {

        game:Phaser.Game;
        emptyHpBar:Phaser.Sprite;
        fullHpBar;
        text:Phaser.Text;

        currentHealth:number;
        maxHealth:number;

        constructor(game:Phaser.Game) {
            this.game = game;

            this.emptyHpBar = this.game.add.sprite(this.game.width * 0.5 - 150, this.game.height - 50, "emptyHpBar");
            this.emptyHpBar.scale.set(1);
            this.fullHpBar = this.game.add.sprite(this.game.width * 0.5 - 150, this.game.height - 50, "fullHpBar");
            this.fullHpBar.scale.set(1);

            var style = {font: "15px Arial", fill: "#ffffff"};
            this.text = this.game.add.text(this.game.width * 0.5, this.game.height - 8, "", style);
            this.text.anchor.set(0.5, 1);
            this.text.alpha = 1;
            this.text.stroke = "rgba(0,0,0,0.6)";
            this.text.strokeThickness = 3;

            this.fullHpBar.fixedToCamera = true;
            this.emptyHpBar.fixedToCamera = true;
            this.text.fixedToCamera = true;
        }

        setCrop() {
            var cropRect = new Phaser.Rectangle(0, 0, (this.currentHealth / this.maxHealth) * this.emptyHpBar.width, this.emptyHpBar.height);
            this.fullHpBar.crop(cropRect);

            if (this.currentHealth >= 0) {
                this.text.setText(this.currentHealth + " / " + this.maxHealth);
            }
            else {
                this.text.setText(0 + " / " + this.maxHealth);
            }

            this.fullHpBar.updateCrop();
        }

        update(player:Player) {
            this.currentHealth = player.currentHealth;
            this.maxHealth = player.maxHealth;
            this.setCrop();

            this.emptyHpBar.bringToTop();
            this.fullHpBar.bringToTop();
            this.text.bringToTop();
        }
    }
}