/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Bullet.ts"/>
/// <reference path="../Classes/Player.ts"/>

module UniGame {
    export class PowerUps {
        game:Phaser.Game;
        sprite:Phaser.Sprite;
        index:number;
        isTaken:boolean;
        aliveTime:number;

        constructor(game:Phaser.Game, spriteKey:string, x:number, y:number) {
            this.index = this.rng(1, 2);
            this.game = game;
            this.sprite = this.game.add.sprite(x, y, spriteKey);
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.scale.set(0.2);

            this.aliveTime = this.game.time.now + 8000;
            this.isTaken = false;
        }

        update(player:Player) {
            if (this.game.physics.arcade.overlap(this.sprite, player.sprite) && !this.isTaken) {
                player.guns[this.index].clipCount++;
                this.isTaken = true;
            }

            if (this.game.time.now > this.aliveTime) {
                this.isTaken = true;
            }
        }

        rng(from, to) {
            return Math.round(Math.random() * (to - from) + from);
        }
    }
}
