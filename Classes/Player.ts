/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Gun.ts"/>
/// <reference path="../Classes/Pistol.ts"/>
/// <reference path="../Classes/Shotgun.ts"/>
/// <reference path="../Classes/Rifle.ts"/>
/// <reference path="../Classes/Unit.ts"/>

module UniGame {
    export class Player extends Unit {
        cursors:Phaser.CursorKeys;
        gun:Gun;
        guns:Array<Gun>;
        killCount:number;

        constructor(game:Phaser.Game, x:number, y:number) {
            super(game, 100, 'player', x, y);
            this.cursors = this.game.input.keyboard.createCursorKeys();
            this.sprite.body.collideWorldBounds = true;
            this.sprite.anchor.set(0.5, 0.5);
            this.sprite.scale.set(0.4, 0.4);
            this.sprite.body.setSize(125, 125);

            this.guns = [];
            this.guns.push(new Pistol(this.game, this));
            this.guns.push(new Rifle(this.game, this));
            this.guns.push(new Shotgun(this.game, this));
            this.gun = this.guns[0];

            this.killCount = 0;
        }

        update(enemies:Array<Unit>) {
            this.move();
            this.gun.update(enemies, this);
            this.sprite.rotation = this.game.physics.arcade.angleToPointer(this.sprite);
            this.gunSelect();
            this.sprite.bringToTop();
        }

        gunSelect() {
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.ONE) && !this.gun.reloading) {
                this.gun = this.guns[0];
            }
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.TWO) && !this.gun.reloading) {
                this.gun = this.guns[1];
            }
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.THREE) && !this.gun.reloading) {
                this.gun = this.guns[2];
            }
        }

        move() {

            if (this.cursors.left.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
                this.sprite.body.velocity.x = -200;
                this.sprite.animations.play('runningLeft');
            }
            else if (this.cursors.right.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
                this.sprite.body.velocity.x = 200;
                this.sprite.animations.play('runningLeft');
            }
            else {
                this.sprite.body.velocity.x = 0;
            }

            if (this.cursors.up.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.W)) {
                this.sprite.body.velocity.y = -200;
                this.sprite.animations.play('runningUp');
            }
            else if (this.cursors.down.isDown || this.game.input.keyboard.isDown(Phaser.Keyboard.S)) {
                this.sprite.body.velocity.y = 200;
                this.sprite.animations.play('runningDown');
            }
            else {
                this.sprite.body.velocity.y = 0;
            }
        }
    }
}