/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Unit.ts"/>
/// <reference path="../Classes/Gun.ts"/>
/// <reference path="../Classes/Player.ts"/>
/// <reference path="../Classes/PowerUps.ts"/>

module UniGame {
    export class Enemy extends Unit {
        gun:Gun;
        hasHit:boolean;
        damage:number;
        hitInterval:number;
        hitTime:number;
        direction:string;
        speed:number;
        path:any;
        newPathTime:number;
        newPathTimeInterval:number;
        animIsDead:boolean;
        hasRolledForPowerUp:boolean;

        attackSound:Phaser.Sound;
        deathSound:Phaser.Sound;

        constructor(game:Phaser.Game, spriteKey:string, x:number, y:number, health:number, speed:number, damage:number) {
            super(game, health, spriteKey, x, y);
            this.sprite.animations.add('walk', [4, 5, 6, 7, 8, 9, 10, 11], 8);
            this.sprite.animations.add('hit', [12, 13, 14, 15], 5);
            this.sprite.animations.add('bite', [16, 17, 18, 19], 8);
            this.sprite.animations.add('die', [22, 23, 24, 25, 26, 27], 8);
            this.sprite.animations.add('critDeath', [28, 29, 30, 31, 32, 33, 34, 35], 8);

            this.damage = damage +19;
            this.hasHit = false;
            this.animIsDead = false;
            this.sprite.body.collideWorldBounds = true;
            this.sprite.anchor.set(0.5, 0.5);
            this.sprite.scale.set(0.7);
            this.sprite.body.setSize(50, 50);
            this.hitTime = this.game.time.now;
            this.hitInterval = 1000;
            this.speed = speed;
            this.direction = "STOP";
            this.hasRolledForPowerUp = false;

            this.newPathTime = this.game.time.now;
            this.newPathTimeInterval = 500;

            this.attackSound = this.game.add.audio('zombieAttackSound', 0.5, false);
            this.deathSound = this.game.add.audio('zombieDeath' + this.rng(1, 8).toString(), 0.05, false);
        }

        update(player:Player, powerUps:Array<PowerUps>) {

            if (!this.isDead) {
                if (this.game.physics.arcade.intersects(this.sprite.body, player.sprite.body)) {
                    if (this.game.time.now > this.hitTime) {
                        this.hitTime = this.game.time.now + this.hitInterval;
                        player.takeDamage(this.damage);
                        this.sprite.animations.play('hit');
                        this.attackSound.play();
                    }
                }

                this.sprite.rotation = this.game.physics.arcade.angleToXY(this.sprite, player.sprite.body.position.x, player.sprite.body.position.y);
                this.move();
            }
            else {
                this.sprite.body.velocity.x = 0;
                this.sprite.body.velocity.y = 0;

                if (!this.hasRolledForPowerUp) {
                    this.deathSound.play();
                    var spawnWeapon = this.rng(1, 3);

                    if (spawnWeapon === 1) {
                        powerUps.push(new PowerUps(this.game, 'ammoPowerUp', this.sprite.body.position.x, this.sprite.position.y));
                    }
                    this.hasRolledForPowerUp = true;
                }
            }
        }

        move() {
            this.sprite.animations.play('walk');
            if (this.direction === "N") {
                this.sprite.body.velocity.y = -this.speed;
                this.sprite.body.velocity.x = 0;
            }
            else if (this.direction === "NW") {
                this.sprite.body.velocity.y = -this.speed;
                this.sprite.body.velocity.x = -this.speed;
            }
            else if (this.direction === "NE") {
                this.sprite.body.velocity.y = -this.speed;
                this.sprite.body.velocity.x = this.speed;
            }
            else if (this.direction === "S") {
                this.sprite.body.velocity.y = this.speed;
                this.sprite.body.velocity.x = 0;
            }
            else if (this.direction === "SE") {
                this.sprite.body.velocity.y = this.speed;
                this.sprite.body.velocity.x = this.speed;
            }
            else if (this.direction === "SW") {
                this.sprite.body.velocity.y = this.speed;
                this.sprite.body.velocity.x = -this.speed;
            }
            else if (this.direction === "E") {
                this.sprite.body.velocity.y = 0;
                this.sprite.body.velocity.x = this.speed;
            }
            else if (this.direction === "W") {
                this.sprite.body.velocity.y = 0;
                this.sprite.body.velocity.x = -this.speed;
            }
            else if (this.direction === "STOP") {
                this.sprite.body.velocity.x = 0;
                this.sprite.body.velocity.y = 0;
            }
        }

        calculatePath(player:Player) {

            var currentNextPointX;
            var currentNextPointY;
            if (this.path && this.path[1] != undefined) {
                currentNextPointX = this.path[1].x;
                currentNextPointY = this.path[1].y;
            }

            if (currentNextPointX == Math.floor(this.sprite.body.position.x / 40) && currentNextPointY < Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "N";
            }
            else if (currentNextPointX < Math.floor(this.sprite.body.position.x / 40) && currentNextPointY == Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "W";
            }
            else if (currentNextPointX > Math.floor(this.sprite.body.position.x / 40) && currentNextPointY == Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "E";
            }
            else if (currentNextPointX == Math.floor(this.sprite.body.position.x / 40) && currentNextPointY > Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "S";
            }
            else if (currentNextPointX > Math.floor(this.sprite.body.position.x / 40) && currentNextPointY > Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "SE";
            }
            else if (currentNextPointX < Math.floor(this.sprite.body.position.x / 40) && currentNextPointY > Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "SW";
            }
            else if (currentNextPointX > Math.floor(this.sprite.body.position.x / 40) && currentNextPointY < Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "NE";
            }
            else if (currentNextPointX < Math.floor(this.sprite.body.position.x / 40) && currentNextPointY < Math.floor(this.sprite.body.position.y / 40)) {
                this.direction = "NW";
            }
            else {
                this.direction = "STOP";
            }
        }

        rng(from, to) {
            return Math.round(Math.random() * (to - from) + from);
        }

    }
}