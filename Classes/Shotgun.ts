/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="../Classes/Bullet.ts"/>
/// <reference path="../Classes/Player.ts"/>

module UniGame {
    export class Shotgun extends Gun {

        name:string;
        bulletSprite:string;
        damage:number;
        fireRate:number;
        maxBulletCount:number;
        reloadSpeed:number;
        bulletSpeed:number;
        bulletAccuracy:number;

        bulletShells:number;
        bulletAngle:number;

        constructor(game:Phaser.Game, player:Player) {
            this.name = "Shotgun";
            this.bulletSprite = 'bullet1';
            this.damage = 23;
            this.fireRate = 1250;
            this.maxBulletCount = 8;
            this.reloadSpeed = 2500;
            this.bulletSpeed = 1500;
            this.bulletAccuracy = 2;

            this.bulletShells = 6;
            this.bulletAngle = 5;
            super(game, this.name, this.bulletSprite, this.damage, this.fireRate, this.maxBulletCount, this.reloadSpeed, this.bulletSpeed, this.bulletAccuracy, player);
        }

        fire() {
            if (this.game.time.now > this.nextFire) {
                this.nextFire = this.game.time.now + this.fireRate;
                if (this.bulletCount > 0) {
                    this.shotSound.play();
                    for (var i = 0; i < this.bulletShells; i++) {
                        var accuracyRotation = (Math.PI / 180) * this.rng(-this.bulletAccuracy, this.bulletAccuracy);
                        var shellRotation = (Math.PI / 180) * ((-this.bulletShells / 2 * this.bulletAngle) + this.bulletAngle * i) + accuracyRotation;
                        this.firedBullets.push(new Bullet(this.game, this.bulletSprite, this.bulletSpeed, this.damage, this.owner, shellRotation));
                    }
                    this.bulletCount--;
                }
                if (this.bulletCount <= 0 && !this.reloading) {
                    this.reload();
                }
            }
        }
    }
}