/// <reference path="../Lib/phaser/phaser.d.ts"/>

module UniGame {
    export class Unit {
        maxHealth:number;
        currentHealth:number;
        game:Phaser.Game;
        sprite;
        isDead:boolean;

        constructor(game:Phaser.Game, maxHealth:number, spriteKey:string, x:number, y:number) {
            this.maxHealth = maxHealth;
            this.currentHealth = maxHealth;
            this.game = game;
            this.sprite = this.game.add.sprite(x, y, spriteKey,0);
            this.game.physics.arcade.enable(this.sprite);
            this.isDead = false;
           // console.log(this.sprite.position.x+"  "+this.sprite.position.y);
            //console.log(this.game.world.bounds);
        }

        takeDamage(value:number) {
            this.currentHealth -= value;
            if (this.currentHealth <= 0) {
                this.currentHealth = 0;
                this.isDead = true;
            }
        }
    }
}
