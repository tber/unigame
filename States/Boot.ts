/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="Preload.ts"/>

module UniGame {
    export class Boot extends Phaser.State {

        preload() {
            this.game.load.image("blackScreen", "assets/Graphics/blackScreen.jpg");
            this.game.load.image("zaribaLogo", "assets/Graphics/zaribaLogo.png");

        }

        create() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'blackScreen');

            this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
            this.game.time.advancedTiming = true;

            var bootLogo1 = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "zaribaLogo");
            bootLogo1.anchor.set(0.5, 0.5);
            bootLogo1.alpha = 0;

            var fadeInTween = this.game.add.tween(bootLogo1).to({alpha: 1}, 250, "Linear", true, 250, 0);
            var fadeOutTween = this.game.add.tween(bootLogo1).to({alpha: 0}, 100, "Linear", false, 1000, 0);

            fadeInTween.onComplete.add(()=> {
                fadeOutTween.start();
            });
            fadeOutTween.onComplete.add(()=> {
                this.game.state.start("Preload");
            });
        }
    }
}