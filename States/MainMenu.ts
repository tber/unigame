/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="Game.ts"/>

module UniGame {
    export class MainMenu extends Phaser.State {

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainMenuBackground');
        }

        create() {
            this.addTitle(125, 'Zombie Rush');

            this.addMenuOption(325, 'Play', this.play, "#E6A12E", 5);
            this.addMenuOption(475, 'Statistics', this.statistics, "#E6A12E", 5);
            this.addMenuOption(625, 'Exit', this.exit, "white", 3);
        }

        addTitle(height, text) {
            var optionStyle = {font: '80pt Arial', fill: '#B6352D'};
            var txt = this.game.add.text(this.game.width * 0.5, height, text, optionStyle);
            txt.stroke = "rgba(255,255,255,0.6)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }

        addMenuOption(height, text, callback, fill, stroke) {
            var optionStyle = {font: '35pt Arial', fill: fill};
            var txt = this.game.add.text(this.game.width * 0.5, height, text, optionStyle);
            txt.anchor.set(0.5);

            txt.stroke = "rgba(255,25,25,0.6)";
            txt.strokeThickness = stroke;
            txt.inputEnabled = true;
            txt.events.onInputDown.add(callback, this);
        }

        play() {
            this.game.state.start("Game");
        }

        statistics()
        {
            this.game.state.start("Statistics");
        }

        exit() {
            this.game.destroy();
        }
    }

}