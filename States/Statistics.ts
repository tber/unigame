/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="MainMenu.ts"/>

module UniGame {
    export class Statistics extends Phaser.State {
        killCount:number;

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainMenuBackground');

            var bloodPart = this.game.add.image(this.game.width * 0.45, this.game.height * 0.5, 'bloodPart');
            bloodPart.anchor.set(0.5);
            bloodPart.alpha = 0.7;

            this.addTitle(100, "Statistics");

            this.addSubTitle(400, 200, "Name");
            this.addSubTitle(850, 200, "Score");

            var array = JSON.parse(localStorage.getItem("array"));

            var j = 0;

            array.sort(function (a,b) {
                return parseInt(b.score) - parseInt(a.score);
            });
            for (var i in array) {
                if (j < 3) {
                    this.addText(400, 300 + j * 100, array[i].username)
                    this.addText(850, 300 + j * 100, array[i].score);
                    j++;
                }
            }


            this.addMenuOption(670, 'Back', this.toMainMenu, "#E6A12E", 5);


            // var style = {font: '45pt Arial', fill: '#792509'};
            // var monsterText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.45, "Zombies killed: " + this.killCount, style);
            // monsterText.stroke = "rgba(255,255,255)";
            // monsterText.strokeThickness = 2;
            // monsterText.anchor.set(0.5);
            //
            // if (!localStorage.getItem("zHighscore")) {
            //     localStorage.setItem("zHighscore", "0")
            // }
            //
            // var highScore = localStorage.getItem("zHighscore");
            // if (this.killCount > highScore) {
            //     localStorage.setItem("zHighscore", this.killCount.toString());
            //     this.newRecord(this.game.height * 0.6);
            // }

            // Click to Exit
            // var fadedBackground = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "blackScreen");
            // fadedBackground.anchor.setTo(0.5);
            // fadedBackground.alpha = 0;
            // fadedBackground.inputEnabled = true;
            // fadedBackground.events.onInputDown.add(this.toMainMenu, this);
        }

        addTitle(height, text) {
            var optionStyle = {font: '80pt Arial', fill: '#B6352D'};
            var txt = this.game.add.text(this.game.width * 0.5, height, text, optionStyle);
            txt.stroke = "rgba(0,0,0,0.8)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }

        addSubTitle(width, height, text) {
            var optionStyle = {font: '50pt Arial', fill: '#B6352D'};
            var txt = this.game.add.text(width, height, text, optionStyle);
            txt.stroke = "rgba(0,0,0,0.8)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }

        addText(width, height, text) {
            var optionStyle = {font: '35pt Arial', fill: '#B6352D'};
            var txt = this.game.add.text(width, height, text, optionStyle);
            txt.stroke = "rgba(0,0,0,0.8)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }

        newRecord(height) {
            var optionStyle = {font: '30px Arial', fill: '#D0A82D'};
            var image = this.game.add.image(this.game.width * 0.5, height - 5, 'star');
            image.anchor.set(0.5);
            image.scale.set(0.5);
            image.alpha = 0.7;
            var txt = this.game.add.text(this.game.width * 0.5, height, "New Record!", optionStyle);
            txt.stroke = "rgba(0,0,0,0.8)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }

        toMainMenu() {
            this.game.state.start("MainMenu", true, false);
        }

        addMenuOption(height, text, callback, fill, stroke) {
            var optionStyle = {font: '35pt Arial', fill: fill};
            var txt = this.game.add.text(100, height, text, optionStyle);
            txt.anchor.set(0.5);

            txt.stroke = "rgba(255,25,25,0.6)";
            txt.strokeThickness = stroke;
            txt.inputEnabled = true;
            txt.events.onInputDown.add(callback, this);
        }
    }
}