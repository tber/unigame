/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="MainMenu.ts"/>

module UniGame {
    export class GameOver extends Phaser.State {
        killCount:number;

        init(killCount:number) {
            this.killCount = killCount;
        }

        preload() {

            var username = prompt("Game Over. Please Enter your name:","Guest");
            if(!username)
            {
                username = "Sucker";
            }
            console.log(username);
            this.game.add.tileSprite(0, 0, 1280, 720, 'mainMenuBackground');

            var bloodPart = this.game.add.image(this.game.width * 0.45, this.game.height * 0.5, 'bloodPart');
            bloodPart.anchor.set(0.5);
            bloodPart.alpha = 0.7;

            this.addTitle(100, "You Died");

            var style = {font: '45pt Arial', fill: '#792509'};
            var monsterText = this.game.add.text(this.game.width * 0.5, this.game.height * 0.45, "Zombies killed: " + this.killCount, style);
            monsterText.stroke = "rgba(255,255,255)";
            monsterText.strokeThickness = 2;
            monsterText.anchor.set(0.5);

            if (!localStorage.getItem("zHighscore")) {
                localStorage.setItem("zHighscore", "0")
            }

            if (!localStorage.getItem("array")) {
                localStorage.setItem("array", "[]");
            }

            var array = JSON.parse(localStorage.getItem("array")) || [];
            var user = null;
            for (var i in array) {
                if (array.hasOwnProperty(i) && array[i].username === username ){
                    user = array[i];
                }
            }

            if (!user) {
                array.push({
                    username: username,
                    score: this.killCount
                })
            } else {
                if (user.score < this.killCount) {
                    user.score = this.killCount;
                }
            }

            localStorage.setItem('array', JSON.stringify(array));
            
            var highScore = localStorage.getItem("zHighscore");
            if (this.killCount > highScore) {
                highScore  = this.killCount;
                this.newRecord(this.game.height * 0.6);
            }

            localStorage.setItem("zHighscore", highScore.toString());
            console.log("hello, It's me");


            // Click to Exit
            var fadedBackground = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "blackScreen");
            fadedBackground.anchor.setTo(0.5);
            fadedBackground.alpha = 0;
            fadedBackground.inputEnabled = true;
            fadedBackground.events.onInputDown.add(this.toMainMenu, this);
        }

        addTitle(height, text) {
            var optionStyle = {font: '80pt Arial', fill: '#B6352D'};
            var txt = this.game.add.text(this.game.width * 0.5, height, text, optionStyle);
            txt.stroke = "rgba(0,0,0,0.8)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }

        newRecord(height) {
            var optionStyle = {font: '30px Arial', fill: '#D0A82D'};
            var image = this.game.add.image(this.game.width * 0.5, height - 5, 'star');
            image.anchor.set(0.5);
            image.scale.set(0.5);
            image.alpha = 0.7;
            var txt = this.game.add.text(this.game.width * 0.5, height, "New Record!", optionStyle);
            txt.stroke = "rgba(0,0,0,0.8)";
            txt.strokeThickness = 3;
            txt.anchor.set(0.5);
        }

        toMainMenu() {
            this.game.state.start("MainMenu", true, false);
        }
    }
}