/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="MainMenu.ts"/>

module UniGame {
    export class Preload extends Phaser.State {

        preload() {
            this.game.add.tileSprite(0, 0, 1280, 720, 'blackScreen');

            this.game.load.image("mainMenuBackground", "assets/Graphics/mainMenuBackground.png");
            this.game.load.image('player', 'assets/Graphics/GameGraphics/BrownAssault.png');
            this.game.load.image('bullet1', 'assets/Graphics/GameGraphics/bullet1.png');
            this.game.load.image('bullet2', 'assets/Graphics/GameGraphics/bullet2.png');
            this.game.load.image('bullet3', 'assets/Graphics/GameGraphics/bullet3.png');
            this.game.load.image('star', 'assets/Graphics/star.png');
            this.game.load.image('bloodPart', 'assets/Graphics/bloodPart.png');
            this.game.load.image('emptyHpBar', 'assets/Graphics/GUI/emptyHpBar.png');
            this.game.load.image('fullHpBar', 'assets/Graphics/GUI/fullHpBar.png');
            this.game.load.image('ammo', 'assets/Graphics/GUI/ammo.png');
            this.game.load.image('Pistol', 'assets/Graphics/GUI/Pistol.png');
            this.game.load.image('Rifle', 'assets/Graphics/GUI/Rifle.png');
            this.game.load.image('Shotgun', 'assets/Graphics/GUI/Shotgun.png');
            this.game.load.image('Clip', 'assets/Graphics/GUI/Clip.png');
            this.game.load.image('skull', 'assets/Graphics/GUI/skull.png');
            this.load.tilemap('level1', 'assets/game.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.image('gameTiles', 'assets/gameTiles.bmp');
            this.load.image('cliff', 'assets/cliff.png');
            this.game.load.spritesheet('zombie', 'assets/Graphics/GameGraphics/zombie.png', 128, 128, 36);
            this.game.load.image('ammoPowerUp', 'assets/Graphics/GameGraphics/ammo.png');
            this.game.load.audio('PistolShot','assets/Sounds/pistolSound.wav');
            this.game.load.audio('RifleShot','assets/Sounds/rifleSound.wav');
            this.game.load.audio('ShotgunShot','assets/Sounds/shotgunSound.wav');
            this.game.load.audio('zombieAttackSound','assets/Sounds/Zombie/zombieAttackSound.wav');
            this.game.load.audio('zombieDeath1','assets/Sounds/Zombie/Zombie_21.wav');
            this.game.load.audio('zombieDeath2','assets/Sounds/Zombie/Zombie_22.wav');
            this.game.load.audio('zombieDeath3','assets/Sounds/Zombie/Zombie_23.wav');
            this.game.load.audio('zombieDeath4','assets/Sounds/Zombie/Zombie_24.wav');
            this.game.load.audio('zombieDeath5','assets/Sounds/Zombie/Zombie_25.wav');
            this.game.load.audio('zombieDeath6','assets/Sounds/Zombie/Zombie_26.wav');
            this.game.load.audio('zombieDeath7','assets/Sounds/Zombie/Zombie_27.wav');
            this.game.load.audio('zombieDeath8','assets/Sounds/Zombie/Zombie_28.wav');
        }

        create() {
            this.game.time.events.add(250, ()=> {
                this.game.state.start("MainMenu");
            }, this);
        }
    }
}