/// <reference path="../Lib/phaser/phaser.d.ts"/>
/// <reference path="GameOver.ts"/>
/// <reference path="../Classes/Player.ts"/>
/// <reference path="../Classes/Enemy.ts"/>
/// <reference path="../Classes/Unit.ts"/>
/// <reference path="../Classes/PowerUps.ts"/>
/// <reference path="../Classes/UI.ts"/>

module UniGame {
    export class Game extends Phaser.State {
        player:Player;
        enemies:Array<Enemy>;
        powerUps:Array<PowerUps>;
        UI:UI;

        pathfinder:any;
        blocked:boolean;
        path:any;

        enemySpawnTime:number;
        enemySpawnInterval:number;
        gameTimer:Phaser.Timer;

        map:Phaser.Tilemap;
        backgroundLayer:Phaser.TilemapLayer;
        wallLayer:Phaser.TilemapLayer;

        preload() {
            this.game.stage.backgroundColor = '#243136';

            // For FPS
            this.game.time.advancedTiming = true;
        }

        create() {
            this.blocked = false;

            // Adding Tile Layers
            this.map = this.game.add.tilemap('level1');
            // the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
            this.map.addTilesetImage('gameTiles', 'gameTiles');
            this.map.addTilesetImage('CLIFFVEG', 'cliff');
            this.backgroundLayer = this.map.createLayer('BackgroundLayer');
            this.wallLayer = this.map.createLayer('WallLayer');
            this.map.setCollisionBetween(1, 1000, true, 'WallLayer');
            //this.game.world.setBounds(, 0, 2400, 2400);
           this.backgroundLayer.resizeWorld();

            // Path finding
            var walkables = [-1];
            this.pathfinder = this.game.plugins.add(Phaser.Plugin.PathFinderPlugin);
            this.pathfinder.setGrid(this.map.layers[1].data, walkables);
            this.pathfinder.diagonalsEnabled = true;

            // Physics
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 0;

            // Player
            this.player = new Player(this.game, this.game.world.width * 0.5, this.game.world.height * 0.5);
            this.game.camera.follow(this.player.sprite);

            // Player UI
            this.UI = new UI(this.game);

            // Groups
            this.enemies = [];
            this.powerUps = [];

            // Enemy Spawn
            this.enemySpawnTime = this.game.time.now;
            this.enemySpawnInterval = 2000;

            // Timer
            this.gameTimer = this.game.time.create(false);
            this.gameTimer.start();
        }


        update() {
            this.player.update(this.enemies);
            this.game.physics.arcade.collide(this.player.sprite, this.wallLayer);

            this.calculateDirection();

            if (this.game.time.now > this.enemySpawnTime) {
                this.enemySpawnTime = this.game.time.now + this.enemySpawnInterval;
                this.spawnEnemy();
            }

            this.updateUI();
            this.updateEnemies();
            this.updatePowerUps();

            this.player.gun.firedBullets.forEach(function (bullet) {
                if (this.game.physics.arcade.collide(bullet.sprite, this.wallLayer)) {
                    bullet.hasHit = true;
                }
            }, this);

            if (this.player.isDead) {
                this.gameOver();
                this.gameTimer.stop();
            }
        }

        gameOver() {
            this.game.time.events.add(500, ()=> {
                this.game.state.clearCurrentState();
                this.game.state.start("GameOver", false, false, this.player.killCount);
            })
        }

        spawnEnemy() {
            var spawnPositionX;
            var spawnPositionY;
            var spawnSide = this.rng(1, 4);

            switch (spawnSide) {
                case 1:
                    spawnPositionX = 0;
                    spawnPositionY = this.rng(0, 59);
                    break;
                case 2:
                    spawnPositionX = 59;
                    spawnPositionY = this.rng(0, 59);
                    break;
                case 3:
                    spawnPositionX = this.rng(0, 59);
                    spawnPositionY = 0;
                    break;
                case 4:
                    spawnPositionX = this.rng(0, 59);
                    spawnPositionY = 59;
                    break;
            }

            spawnPositionX = spawnPositionX * 40;
            spawnPositionY = spawnPositionY * 40;

            var enemy = new Enemy(this.game, 'zombie', spawnPositionX, spawnPositionY, 50 + Math.floor(this.gameTimer.ms / 1000 / 5),
                100 + Math.floor(this.gameTimer.ms / 1000 / 2) + this.enemies.length * 2, 5 + Math.floor(this.gameTimer.ms / 1000 / 10));
            this.enemies.push(enemy);
        }

        calculateDirection() {
            this.enemies.forEach((enemy)=> {
                if (!enemy.isDead) {
                    if (this.game.time.now > enemy.newPathTime) {
                        enemy.newPathTime = this.game.time.now + enemy.newPathTimeInterval;
                        this.blocked = true;
                        this.findPathTo(Math.floor(this.player.sprite.body.position.x / 40), Math.floor(this.player.sprite.body.position.y / 40), enemy);
                    }
                }
            });
        }

        findPathTo(tilex, tiley, enemy) {
            this.pathfinder.setCallbackFunction(function (path) {
                path = path || [];
                enemy.path = path;
                enemy.calculatePath(this.player);
                this.blocked = false;

            });

            this.pathfinder.preparePathCalculation([Math.floor(enemy.sprite.body.position.x / 40), Math.floor(enemy.sprite.body.position.y / 40)], [tilex, tiley]);
            this.pathfinder.calculatePath();
        }

        updateUI() {
            this.UI.update(this.player);
        }

        updateEnemies() {
            this.enemies.forEach((enemy)=> {
                enemy.update(this.player, this.powerUps);
                if (enemy.isDead && !enemy.animIsDead) {
                    enemy.sprite.lifespan = 2000;
                    var rngAnim = this.rng(1, 2);
                    if (rngAnim === 1) {
                        enemy.sprite.animations.play('die');
                    }
                    else {
                        enemy.sprite.animations.play('critDeath');
                    }
                    enemy.animIsDead = true;

                    this.player.killCount++;
                }
                if (!enemy.sprite.alive) {
                    enemy.sprite.destroy();
                    this.enemies.splice(this.enemies.lastIndexOf(enemy), 1);
                }
            });
        }

        updatePowerUps() {
            this.powerUps.forEach((powerUp)=> {
                powerUp.update(this.player);
                if (powerUp.isTaken) {
                    powerUp.sprite.destroy();
                    this.powerUps.splice(this.powerUps.lastIndexOf(powerUp), 1);
                }
            });
        }

        rng(from, to) {
            return Math.round(Math.random() * (to - from) + from);
        }

        render() {
            //this.game.debug.text(this.game.time.fps.toString() || '--', 5, 14, "#FFFFFF");
            //
            //this.game.debug.body(this.player.sprite, 'rgba(189, 221, 235, 0.6)', false);
            //
            //this.enemies.forEach((enemy)=> {
            //    this.game.debug.body(enemy.sprite, 'rgba(189, 221, 235, 0.6)', false);
            //});
            //
            //this.powerUps.forEach((powerUp)=> {
            //    this.game.debug.body(powerUp.sprite, 'rgba(189, 221, 235, 0.6)', false);
            //});
            //
            //this.player.gun.firedBullets.forEach((item)=> {
            //    this.game.debug.body(item.sprite, 'rgba(50, 235, 50, 1)', false);
            //});
        }
    }
}